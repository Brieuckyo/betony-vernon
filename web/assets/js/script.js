// Swiper
var swiper = new Swiper('.swiper-container', {
	centeredSlides: true,
	loop: true,
	autoplay: true,
	effect: 'fade',
	speed: 1000,
	spaceBetween: 3000,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
});
