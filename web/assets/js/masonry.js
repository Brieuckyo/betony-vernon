var elem = document.querySelector('.grid');
var msnry = new Masonry(elem, {
	itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
	columnWidth: '.grid-sizer',
	// horizontalOrder: true,
	percentPosition: true
});

window.onload = () => {
	console.log('image loaded')
	msnry.layout()
	// setTimeout(() => {
	//
	// }, 500)
}

window.addEventListener('resize', () => {
	msnry.reloadItems()
})

